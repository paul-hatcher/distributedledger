/*
 * DSHA256Block.h
 *
 *  Created on: Oct 26, 2018
 *      Author: paul
 */

#ifndef DSHA256BLOCK_H_
#define DSHA256BLOCK_H_

#include "SHA256Context.h"
#include "CudaUtils.h"

class CDSHA256Block {
public:

    void run_sync(CSHA256Context& context, uint32_t digest[SHA256_DIGEST_WORDS]);

    enum {MAX_BLOCKS=1024};
private:
    CCudaGlobalMemoryTempl<CSHA256Context> context_;
    CCudaGlobalMemoryTempl<uint32_t[MAX_BLOCKS][SHA256_DIGEST_WORDS]> digest_;
    CCudaEvent start_, stop_;
};

#endif /* DSHA256BLOCK_H_ */
