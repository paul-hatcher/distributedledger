/*
 * SHA256Context.cpp
 *
 *  Created on: Oct 23, 2018
 *      Author: paul
 */

#include <cstring>
#include <iomanip>

#include "SHA256Context.h"

CSHA256Context::CSHA256Context() {
    memset((char*)input_, 0, sizeof(input_));
}

void CSHA256Context::set(const char* const sz) {
    memset((char*)input_, 0, sizeof(input_));
    const uint32_t nLength = strnlen(sz, sizeof(input_)-8);
    strncpy((char*)input_, sz, nLength);
    char* p = (char*)input_;
    p[nLength] = 0x80;
    p[sizeof(input_)-1] = ((nLength << 3) & 0x0FF);
    p[sizeof(input_)-2] = ((nLength >> 5) & 0x0FF);
}

void CSHA256Context::dump_input(std::ostream& os) const {
    const uint32_t nBytePerRow=16;
    uint8_t* p = (uint8_t*)input_;
    for (uint32_t k=0; k<(SHA256_BLOCK_SIZE/nBytePerRow); ++k) {
        for (uint32_t j=0; j<nBytePerRow; ++j) {
            os << std::setfill('0') << std::setw(2) << std::hex << uint32_t(*(p++)) << ' ';
        }
        os << std::endl;
    }
}

void CSHA256Context::dump_digest(std::ostream& os) const {
    dump_digest(os, digest_);
}

void CSHA256Context::dump_digest(std::ostream& os, const uint32_t digest[SHA256_DIGEST_WORDS]) {
    for (uint32_t i=0; i<SHA256_DIGEST_WORDS; ++i){
        os << std::setfill('0') << std::setw(8) << std::hex << digest[i];
    }
    os << std::endl;
}

std::ostream& operator << (std::ostream& os, const CSHA256Context& c) {
    os << "input:" << std::endl;
    c.dump_input(os);
    os << "digest:" << std::endl;
    c.dump_digest(os);
    return os;
}

std::ostream& operator << (std::ostream& os, const uint32_t digest[SHA256_DIGEST_WORDS]) {
    os << "digest:" << std::endl;
    CSHA256Context::dump_digest(os,digest);
    return os;
}
