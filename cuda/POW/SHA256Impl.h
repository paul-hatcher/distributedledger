/*
 * SHA256Impl.h
 *
 *  Created on: Oct 26, 2018
 *      Author: paul
 *
 *      Timplementation macros for the kernel code.
 */

#ifndef SHA256IMPL_H_
#define SHA256IMPL_H_

#include "SHA256Const.h"

#if defined (__CUDACC__)
#define bswap(x) (__byte_perm(x,0,0x0123))
#else
#define bswap(x) (((x)<<24) | (((x)&0xff00)<<8) | (((x)&0xff0000)>>8) | ((x)>>24))
#endif

#if defined (__CUDACC__)
#define ror32(x, r) (__funnelshift_r(x,x,r))
#else
#define ror32(x, r) (((x)>>(r)) ^ ((x)<<(32-(r))))
#endif

#define S0(w) (ror32(w,7) ^ ror32(w,18) ^ (w >> 3))
#define S1(w) (ror32(w,17) ^ ror32(w,19) ^ (w >> 10))

#define s0(a) (ror32(a,2) ^ ror32(a,13) ^ ror32(a,22))
#define s1(e) (ror32(e,6) ^ ror32(e,11) ^ ror32(e,25))
#define maj(a,b,c) ((a & b) ^ (a & c) ^ (b & c))
#define ch(e,f,g) ((e & f) ^ (g & ~e))

// store_w is only used for step 0 ~ 15
#define store_w(i) w[i] = ww[i];
#define Ws(i)      w[(i) & 15]

// update_w is used for step > 15
#define update_w(i) Ws(i) = Ws(i-16) + S0(Ws(i-15)) + Ws(i-7) + S1(Ws(i-2));

#define update_t2(a, b, c)          const uint32_t t2 = s0(a) + maj(a,b,c);
#define update_t1(h, e, f, g, i, k) const uint32_t t1 = h + s1(e) + ch(e,f,g) + k[i] + Ws(i);

#if 0// defined(OPTIMIZE_00)
#define step(s_u, i, digest, k) \
{ \
    s_u (i); \
          uint32_t& a = digest[(i+0) & 7];\
    const uint32_t  b = digest[(i+1) & 7];\
    const uint32_t  c = digest[(i+2) & 7];\
    update_t2(a, b, c); \
    \
    const uint32_t  d = digest[(i+3) & 7];\
          uint32_t& e = digest[(i+4) & 7];\
    const uint32_t  f = digest[(i+5) & 7];\
    const uint32_t  g = digest[(i+6) & 7];\
    const uint32_t  h = digest[(i+7) & 7];\
    update_t1(h, e, f, g, i, k); \
    \
    a = t1 + t2; \
    e = d  + t2; \
}
#define STEP(s_u, i, digest, kk) \
        step(s_u, i*8 + 0, digest, kk );\
        step(s_u, i*8 + 1, digest, kk );\
        step(s_u, i*8 + 2, digest, kk );\
        step(s_u, i*8 + 3, digest, kk );\
        step(s_u, i*8 + 4, digest, kk );\
        step(s_u, i*8 + 5, digest, kk );\
        step(s_u, i*8 + 6, digest, kk );\
        step(s_u, i*8 + 7, digest, kk );
#else
#define step(s_u,i,a,b,c,d,e,f,g,h,k) \
{ \
    s_u (i); \
    update_t2(a, b, c); \
    update_t1(h, e, f, g, i, k); \
    h = g;\
    g = f;\
    f = e;\
    e = d + t1;\
    d = c;\
    c = b;\
    b = a;\
    a = t1 + t2;\
}
#define STEP(s_u, i, digest, kk) \
        step(s_u, i*8 + 0, aa, bb, cc, dd, ee, ff, gg, hh, kk );\
        step(s_u, i*8 + 1, aa, bb, cc, dd, ee, ff, gg, hh, kk );\
        step(s_u, i*8 + 2, aa, bb, cc, dd, ee, ff, gg, hh, kk );\
        step(s_u, i*8 + 3, aa, bb, cc, dd, ee, ff, gg, hh, kk );\
        step(s_u, i*8 + 4, aa, bb, cc, dd, ee, ff, gg, hh, kk );\
        step(s_u, i*8 + 5, aa, bb, cc, dd, ee, ff, gg, hh, kk );\
        step(s_u, i*8 + 6, aa, bb, cc, dd, ee, ff, gg, hh, kk );\
        step(s_u, i*8 + 7, aa, bb, cc, dd, ee, ff, gg, hh, kk );

#endif

#define SHA256_BLOCK(func, ctx) \
void func (const ctx& context, uint32_t digest[SHA256_DIGEST_WORDS]) {\
\
    const uint32_t*const ww = context.getInput();\
    uint32_t w[SHA256_BLOCK_SIZE/sizeof(uint32_t)];\
    const uint32_t *const cdigest=context.getDigest();\
\
    uint32_t aa,bb,cc,dd,ee,ff,gg,hh;\
\
    __constant__\
    const static uint32_t kk[64] = {\
        0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,\
        0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,\
        0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,\
        0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,\
        0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,\
        0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,\
        0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,\
        0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,\
        0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,\
        0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,\
        0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3,\
        0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,\
        0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,\
        0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,\
        0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,\
        0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2\
    };\
\
    aa = cdigest[0];\
    bb = cdigest[1];\
    cc = cdigest[2];\
    dd = cdigest[3];\
    ee = cdigest[4];\
    ff = cdigest[5];\
    gg = cdigest[6];\
    hh = cdigest[7];\
\
    STEP(store_w, 0, digest, kk)\
    STEP(store_w, 1, digest, kk)\
    STEP(update_w, 2, digest, kk)\
    STEP(update_w, 3, digest, kk)\
    STEP(update_w, 4, digest, kk)\
    STEP(update_w, 5, digest, kk)\
    STEP(update_w, 6, digest, kk)\
    STEP(update_w, 7, digest, kk)\
\
    digest[0] = cdigest[0] + aa;\
    digest[1] = cdigest[1] + bb;\
    digest[2] = cdigest[2] + cc;\
    digest[3] = cdigest[3] + dd;\
    digest[4] = cdigest[4] + ee;\
    digest[5] = cdigest[5] + ff;\
    digest[6] = cdigest[6] + gg;\
    digest[7] = cdigest[7] + hh;\
}

#endif /* SHA256IMPL_H_ */
