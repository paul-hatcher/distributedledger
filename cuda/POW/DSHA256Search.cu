/*
 * DSHA256Search.cpp
 *
 *  Created on: Oct 26, 2018
 *      Author: paul
 */

#include "DSHA256Search.h"
#include "DSHA256Context.h"
#include "SHA256Impl.h"

namespace NDSHA256Search {

__device__
inline
SHA256_BLOCK(SHA256Block, CDSHA256Context)

__global__
void run(CSHA256Context* pContext, CDSHA256Search::SHA256Nonce nonces[CDSHA256Search::MAX_BLOCKS]) {

    // In shared memory.
    __shared__
    CDSHA256Search::SHA256NonceData nonceBlock;

    // Thread 0 resets memory.
    if (0 == threadIdx.x) {
       nonceBlock.nSize=0;
    }

    // Take a copy of incoming context. Will byte swap input.
    CDSHA256Context context(pContext);

    // Where the result digest is stored.
    uint32_t digest[SHA256_DIGEST_WORDS];

    // We work over a nonce range in this thread.
    // In total all threads test the complete 32 bit range.
    const uint32_t nIterations = (uint32_t(1) << 31) / ((blockDim.x * gridDim.x)/2);
    const uint32_t nonce = nIterations * ((blockIdx.x * blockDim.x) + threadIdx.x);

    // All threads should reach this common point, prior to main loop.
    __syncthreads();

    for (uint32_t i=0; i<nIterations; ++i) {

        context.set(nonce+i, 3);

        // Perform a single pass.
        SHA256Block(context, digest);

        // Check first word. If 0, save nonce.
        if (0 == digest[0]) {
            nonceBlock.nonces[nonceBlock.nSize++] = nonce+i;
        }
    }

    // All threads must have finished.
    __syncthreads();

    // Copy to global memory if first thread in the block.
    if (0==threadIdx.x) {
        nonces[blockIdx.x] = nonceBlock;
    }
}

}

void CDSHA256Search::run_sync(CSHA256Context& context, std::vector<uint32_t>& nonces) {

    cudaOccupancy(std::cout, NDSHA256Search::run, 512);
    {
        int minGridSize, blockSize;
        cudaOccupancyMaxPotentialBlockSize (
                &minGridSize,
                &blockSize,
                NDSHA256Search::run
                );
        std::cout << "minGridSize=" << minGridSize << std::endl
          << "blockSize=" << blockSize << std::endl;
    }

    // Copy ourselves to CUDA memory.
    checkCudaErrors(cudaMemcpy(context_.get(), &context, context_.size(), cudaMemcpyHostToDevice));

    CCudaSync sync(start_, stop_);

    // Important to check (clear) errors before and after Kernel launch.
    checkCudaErrors(cudaGetLastError());
    NDSHA256Search::run<<<512,512>>>(context_.get(), nonces_.get()[0]);
    checkCudaErrors(cudaGetLastError());

    // Wait for completion.
    sync.wait();

    SHA256Nonce noncesHost[MAX_BLOCKS];

    // Get hold of results.
    checkCudaErrors(cudaMemcpy(noncesHost, nonces_.get()[0], nonces_.size(), cudaMemcpyDeviceToHost));

    for (uint32_t i=0; i<MAX_BLOCKS; ++i) {
        if (noncesHost[i].nSize) {
            for (uint32_t j=0; j<SHA256Nonce::MAX_NONCES; ++j){
                nonces.push_back(noncesHost[i].nonces[j]);
            }
        }
    }
}
