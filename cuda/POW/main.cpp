#include <time.h>
#include <unistd.h>
#include <iostream>
#include <cstring>
#include <iomanip>
#include <sstream>
#include <vector>

#include <cuda_runtime.h>

#include "DSHA256Block.h"
#include "DSHA256Search.h"

#if 0
// Host code

class CTimer {
public:
    CTimer() {
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time_[0]);
    }
    uint64_t elapsedTimeus() {
        timespec timeNow;
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeNow);
        return
        ((timeNow.tv_sec*1000000L) + (timeNow.tv_nsec/1000L)) -
        ((time_[0].tv_sec*1000000L) + (time_[0].tv_nsec/1000L)) ;
    }
    ~CTimer() {

    }
private:
    timespec time_[2];
};
#endif

int main(int argc, char* argv[])
{
    // Parse command line.
    std::vector<uint32_t> blockCounts;
    std::vector<uint32_t> threadCounts;
    std::vector<uint32_t> iterations;
    bool nLogLevel=0;

    {
        int32_t opt;
        while ((opt = getopt(argc,argv,"b:t:i:l:")) != EOF){

            auto convert=[](std::vector<uint32_t>& v, const char* sz) {
                std::istringstream iss(sz);
                std::string s;
                while (std::getline(iss,s,',')) {
                    v.push_back(std::stoi(s));
                }
            };

            switch (opt){
            case 'b':
            {
                convert(blockCounts, optarg);
                break;
            }
            case 't':
            {
                convert(threadCounts, optarg);
                break;
            }
            case 'i':
            {
                convert(iterations, optarg);
                break;
            }
            case 'l':
            {
                nLogLevel=std::stoi(optarg);
                break;
            }
            default:
                return 1;
            }
        }
    }

    {
        cudaError_t nCudaError;
        nCudaError=cudaSetDevice(0);
        if (nCudaError) {
            return 1;
        }
        uint32_t nFlags;
        nCudaError=cudaGetDeviceFlags(&nFlags);
        if (nCudaError) {
            return 1;
        }
        std::cout << "Flags=" << nFlags << std::endl;
        nCudaError=cudaSetDeviceFlags(cudaDeviceScheduleYield);
        if (nCudaError) {
            return 1;
        }
    }
    {
        CSHA256Context context;
        context.set("abc");
        CDSHA256Block sha256;
        uint32_t digest[SHA256_DIGEST_WORDS];
        sha256.run_sync(context, digest);

        std::cout << context << digest << std::endl;
        //context.dump_digest(std::cout,digest) << std::endl;
        context.set("ab");
        sha256.run_sync(context, digest);
        std::cout << context << digest << std::endl;
    }
    {
        CSHA256Context context;
        context.set("abcdefgh");
        CDSHA256Search sha256Search;
        std::vector<uint32_t> nonces;
        for (uint32_t i=0; (i<8) and (0==nonces.size()); ++i) {
            context.set(std::to_string(i).c_str());
            sha256Search.run_sync(context, nonces);
            std::cout << "i=" << i << "size=" << nonces.size() << std::endl;
        }
    }

#if 0
    CSHA256 sha256;
    sha256.occupancy(std::cout);
    sha256.getContext().set("abcde");
    sha256.kernel_test();
    cudaDeviceSynchronize();
    std::cout << sha256.getContext() << std::endl;

    // Iterate over our arguments.
    for (auto b : blockCounts) {
        for (auto t : threadCounts) {
            for (auto i : iterations) {
                //CTimer timer;
                cudaEvent_t start,stop;
                cudaEventCreate(&start);
                cudaEventCreate(&stop);
                cudaEventRecord(start,0);
                sha256.kernel_search(b,t,i);
                cudaEventRecord(stop,0);
                cudaEventSynchronize(stop);
                float nTimems;
                cudaEventElapsedTime(&nTimems, start, stop);
                const uint64_t nTimeus=nTimems*1000.0;

                if (0 == nLogLevel) {
                    std::cout << b << ',' << t << ',' << i << ',' << (uint64_t(t)*b*i)/nTimeus << std::endl;
                }
                if (nLogLevel > 0){
                    std::cout << "Elapsed Time : " << std::dec << nTimeus << " us. "
                              << "Blocks=" << b
                              << " Threads=" << t
                              << " Iterations=" << i << ' '
                              << (uint64_t(t)*b*i)/nTimeus << " Mh/s."
                              //" Nonces=" << sha256.getContext().getNonceCount()
                              << std::endl;
                }
                if (nLogLevel > 1) {
                    //cudaMemcpy(&context, cContext, sizeof(SHA256Context), cudaMemcpyDeviceToHost);
                    //std::cout << context << std::endl;
                }
            }
        }
    }
#endif
    return 0;
}

