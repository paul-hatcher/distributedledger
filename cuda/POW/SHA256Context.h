/*
 * SHA256Context.h
 *
 *  Created on: Oct 23, 2018
 *      Author: paul
 *
 *      Common SHA256 context data.
 */

#ifndef SHA256CONTEXT_H_
#define SHA256CONTEXT_H_

#include "SHA256Impl.h"

#include <iostream>

class CDSHA256Context;

class CSHA256Context {
public:
    CSHA256Context();

#if defined(__CUDACC__)
public:
    __device__
    inline CSHA256Context(const CSHA256Context* p);
private:
    __device__
    inline void endian_swap(uint32_t* pDst, const uint32_t* pSrc);
public:
#endif

    void set(const char* const sz);

    void dump_input(std::ostream& os) const;
    void dump_digest(std::ostream& os) const;
    static void dump_digest(std::ostream& os, const uint32_t digest[SHA256_DIGEST_WORDS]);

protected:

    uint32_t input_[SHA256_BLOCK_SIZE/sizeof(uint32_t)];
    uint32_t digest_[SHA256_DIGEST_WORDS] = {
        0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A,
        0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19};
};

std::ostream& operator << (std::ostream& os, const CSHA256Context& c);
std::ostream& operator << (std::ostream& os, const uint32_t digest[SHA256_DIGEST_WORDS]);

#if defined(__CUDACC__)
__device__
CSHA256Context::CSHA256Context(const CSHA256Context* p) {
    // Swap input bytes around, copy initial digest.
    endian_swap(input_, p->input_);
    memcpy(digest_, p->digest_, sizeof(digest_));
}
__device__
void CSHA256Context::endian_swap(uint32_t* pDst, const uint32_t* pSrc) {
    for (uint32_t i=0; i<((SHA256_BLOCK_SIZE)/sizeof(uint32_t)); ++i) {
        *pDst++ = bswap(*pSrc++);
    }
}
#endif

#endif /* SHA256CONTEXT_H_ */
