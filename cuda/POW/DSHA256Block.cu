/*
 * DSHA256Block.cpp
 *
 *  Created on: Oct 26, 2018
 *      Author: paul
 */

#include "DSHA256Block.h"
#include "SHA256Impl.h"
#include "DSHA256Context.h"

namespace NDSHA256Block {

__device__
inline
SHA256_BLOCK(SHA256Block, CDSHA256Context)

__global__
void run(CSHA256Context* pContext, uint32_t (*digests)[CDSHA256Block::MAX_BLOCKS][SHA256_DIGEST_WORDS]) {

    // Take a copy of incoming context. Will byte swap input.
    CDSHA256Context context(pContext);

    // Perform a single pass.
    SHA256Block(context, (*digests)[ blockIdx.x ]);
}

}

void CDSHA256Block::run_sync(CSHA256Context& context, uint32_t digest[SHA256_DIGEST_WORDS]) {

    // Copy ourselves to CUDA memory.
    cudaMemcpy(context_.get(), &context, context_.size(), cudaMemcpyHostToDevice);

    CCudaSync sync(start_, stop_);
    NDSHA256Block::run<<<MAX_BLOCKS,1>>>(context_.get(), digest_.get());

    // Wait for completion.
    sync.wait();

    memset(digest, 0, sizeof(digest));

    // Get hold of results.
    cudaMemcpy(digest, digest_.get()[0], digest_.size()/MAX_BLOCKS, cudaMemcpyDeviceToHost);
}
