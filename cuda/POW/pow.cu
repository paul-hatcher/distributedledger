#include <time.h>
#include <unistd.h>
#include <iostream>
#include <cstring>
#include <iomanip>
#include <sstream>
#include <vector>

#define OPTIMIZE_00

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
// Base multi-hash SHA256 Functions
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
#define HASH_SEGS (1)
#define SHA256_DIGEST_WORDS (8)
#define SHA256_BLOCK_SIZE (64)
#define MH_SHA256_BLOCK_SIZE (HASH_SEGS * SHA256_BLOCK_SIZE)

#if defined (OPTIMIZE_00)
#define ror32(x, r) (__funnelshift_r(x,x,r))
#else
#define ror32(x, r) (((x)>>(r)) ^ ((x)<<(32-(r))))
#endif

#if defined (OPTIMIZE_00)
#define bswap(x) (__byte_perm(x,0,0x0123))
#else
#define bswap(x) (((x)<<24) | (((x)&0xff00)<<8) | (((x)&0xff0000)>>8) | ((x)>>24))
#endif

#define S0(w) (ror32(w,7) ^ ror32(w,18) ^ (w >> 3))
#define S1(w) (ror32(w,17) ^ ror32(w,19) ^ (w >> 10))

#define s0(a) (ror32(a,2) ^ ror32(a,13) ^ ror32(a,22))
#define s1(e) (ror32(e,6) ^ ror32(e,11) ^ ror32(e,25))
#define maj(a,b,c) ((a & b) ^ (a & c) ^ (b & c))
#define ch(e,f,g) ((e & f) ^ (g & ~e))

// store_w is only used for step 0 ~ 15
#define store_w(i) w[i] = ww[i];
#define Ws(i)      w[(i) & 15]

// update_w is used for step > 15
#define update_w(i) Ws(i) = Ws(i-16) + S0(Ws(i-15)) + Ws(i-7) + S1(Ws(i-2));

#define update_t2(a, b, c)          const uint32_t t2 = s0(a) + maj(a,b,c);
#define update_t1(h, e, f, g, i, k) const uint32_t t1 = h + s1(e) + ch(e,f,g) + k[i] + Ws(i);

#if defined(OPTIMIZE_00)
#define step(s_u, i, digest, k) \
{ \
    s_u (i); \
          uint32_t& a = digest[(i+0) & 7];\
    const uint32_t  b = digest[(i+1) & 7];\
    const uint32_t  c = digest[(i+2) & 7];\
    update_t2(a, b, c); \
    \
    const uint32_t  d = digest[(i+3) & 7];\
          uint32_t& e = digest[(i+4) & 7];\
    const uint32_t  f = digest[(i+5) & 7];\
    const uint32_t  g = digest[(i+6) & 7];\
    const uint32_t  h = digest[(i+7) & 7];\
    update_t1(h, e, f, g, i, k); \
    \
    a = t1 + t2; \
    e = d  + t2; \
}
#define STEP(s_u, i, digest, kk) \
        step(s_u, i*8 + 0, digest, kk );\
        step(s_u, i*8 + 1, digest, kk );\
        step(s_u, i*8 + 2, digest, kk );\
        step(s_u, i*8 + 3, digest, kk );\
        step(s_u, i*8 + 4, digest, kk );\
        step(s_u, i*8 + 5, digest, kk );\
        step(s_u, i*8 + 6, digest, kk );\
        step(s_u, i*8 + 7, digest, kk );
#else
#define step(s_u,i,a,b,c,d,e,f,g,h,k) \
{ \
    s_u (i); \
	update_t2(a, b, c); \
	update_t1(h, e, f, g, i, k); \
    h = g;\
    g = f;\
    f = e;\
    e = d + t1;\
    d = c;\
    c = b;\
    b = a;\
    a = t1 + t2;\
}
#define STEP(s_u, i) \
        step(s_u, i*8 + 0, aa, bb, cc, dd, ee, ff, gg, hh, kk );\
        step(s_u, i*8 + 1, aa, bb, cc, dd, ee, ff, gg, hh, kk );\
        step(s_u, i*8 + 2, aa, bb, cc, dd, ee, ff, gg, hh, kk );\
        step(s_u, i*8 + 3, aa, bb, cc, dd, ee, ff, gg, hh, kk );\
        step(s_u, i*8 + 4, aa, bb, cc, dd, ee, ff, gg, hh, kk );\
        step(s_u, i*8 + 5, aa, bb, cc, dd, ee, ff, gg, hh, kk );\
        step(s_u, i*8 + 6, aa, bb, cc, dd, ee, ff, gg, hh, kk );\
        step(s_u, i*8 + 7, aa, bb, cc, dd, ee, ff, gg, hh, kk );

#endif

struct SHA256Context {

    SHA256Context() {
        memset((char*)input_, 0, sizeof(input_));
    }

    __device__
    SHA256Context(const SHA256Context& o) {
        endian_swap(input_, o.input_);
        // memcpy(input_, o.input_, sizeof(input_));
        memcpy(digest_, o.digest_, sizeof(digest_));
    }

    __device__
    void reset() {
    }

    __device__
    void set(const quint32 n, const quint32 nOffset) {
        input_[nOffset] = bswap(n);
    }

    void set(const char* const sz) {
        const uint32_t nLength = strnlen(sz, sizeof(input_)-8);
        strncpy((char*)input_, sz, nLength);
        char* p = (char*)input_;
        p[nLength] = 0x80;
        p[sizeof(input_)-1] = ((nLength << 3) & 0x0FF);
        p[sizeof(input_)-2] = ((nLength >> 5) & 0x0FF);
    }

    __device__
    void endian_swap(uint32_t* pDst, const uint32_t* pSrc) {
        for (uint32_t i=0; i<((SHA256_BLOCK_SIZE)/sizeof(uint32_t)); ++i) {
            *pDst++ = bswap(*pSrc++);
        }
    }

    void dump_input(std::ostream& os) const {
        const uint32_t nBytePerRow=16;
        uint8_t* p = (uint8_t*)input_;
        for (uint32_t k=0; k<(SHA256_BLOCK_SIZE/nBytePerRow); ++k) {
            for (uint32_t j=0; j<nBytePerRow; ++j) {
                os << std::setfill('0') << std::setw(2) << std::hex << uint32_t(*(p++)) << ' ';
            }
            os << std::endl;
        }
    }

    void dump_digest(std::ostream& os) const {
        for (uint32_t i=0; i<SHA256_DIGEST_WORDS; ++i){
            os << std::setfill('0') << std::setw(8) << std::hex << digest_[i];
        }
        os << std::endl;
    }

    uint32_t input_[SHA256_BLOCK_SIZE/sizeof(uint32_t)];
    uint32_t digest_[SHA256_DIGEST_WORDS] = {
        0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A,
        0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19};
};

std::ostream& operator << (std::ostream& os, const SHA256Context& c) {
    os << "input:" << std::endl;
    c.dump_input(os);
    os << "digest:" << std::endl;
    c.dump_digest(os);
    return os;
}
/*
 * API to perform 0-64 steps of the multi-hash algorithm for
 * a single block of data. The caller is responsible for ensuring
 * a full block of data input. Source already has bytes swapped.
 *
 * Argument:
 *   input  - the pointer to the data
 *   digest - the space to hold the digests for all segments.
 *
 * Return:
 *   N/A
 */
__device__
void mh_sha256_single(const SHA256Context& context, uint32_t digest[SHA256_DIGEST_WORDS])
{
	const uint32_t *ww = context.input_;
	uint32_t w[SHA256_BLOCK_SIZE/sizeof(uint32_t)];

	__constant__
	const static uint32_t kk[64] = {
		0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,
		0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
		0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
		0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
		0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
		0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
		0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
		0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
		0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
		0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
		0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3,
		0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
		0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,
		0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
		0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
		0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
	};

    digest[0] = context.digest_[0];
    digest[1] = context.digest_[1];
    digest[2] = context.digest_[2];
    digest[3] = context.digest_[3];
    digest[4] = context.digest_[4];
    digest[5] = context.digest_[5];
    digest[6] = context.digest_[6];
    digest[7] = context.digest_[7];

	STEP(store_w, 0, digest, kk)
	STEP(store_w, 1, digest, kk)
	STEP(update_w, 2, digest, kk)
	STEP(update_w, 3, digest, kk)
	STEP(update_w, 4, digest, kk)
	STEP(update_w, 5, digest, kk)
	STEP(update_w, 6, digest, kk)
	STEP(update_w, 7, digest, kk)

    digest[0] += context.digest_[0];
    digest[1] += context.digest_[1];
    digest[2] += context.digest_[2];
    digest[3] += context.digest_[3];
    digest[4] += context.digest_[4];
    digest[5] += context.digest_[5];
    digest[6] += context.digest_[6];
    digest[7] += context.digest_[7];
}

__global__
void mh_sha256_kernel(SHA256Context* cContext, uint32_t nIterations) {

    // Take a copy of incoming context. Will byte swap input.
    SHA256Context context(*cContext);

    const uint32 nId = blockIdx.x

    // Just iterate over given context.
    uint32_t digest[SHA256_DIGEST_WORDS];
    for (uint32_t i=0 ; i<nIterations; ++i) {
        context.reset();
        context.set(0, 3);
        mh_sha256_single(context, digest);
    }

    // Copy back to the cContext.
    if (0==blockIdx.x and 0==threadIdx.x){
        memcpy(cContext->digest_, digest, sizeof(digest));
    }
}

// Host code
int occupancy()
{
    int numBlocks;        // Occupancy in terms of active blocks
    int blockSize = 512;

    // These variables are used to convert occupancy to warps
    int device;
    cudaDeviceProp prop;
    int activeWarps;
    int maxWarps;

    cudaGetDevice(&device);
    cudaGetDeviceProperties(&prop, device);

    cudaOccupancyMaxActiveBlocksPerMultiprocessor(
        &numBlocks,
        mh_sha256_kernel,
        blockSize,
        0);

    activeWarps = numBlocks * blockSize / prop.warpSize;
    maxWarps = prop.maxThreadsPerMultiProcessor / prop.warpSize;

    std::cout << "Occupancy: " << (double)activeWarps / maxWarps * 100 << "%" << std::endl;

    return 0;
}

class CTimer {
public:
	CTimer() {
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time_[0]);
	}
	uint64_t elapsedTimeus() {
		timespec timeNow;
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeNow);
		return
		((timeNow.tv_sec*1000000L) + (timeNow.tv_nsec/1000L)) -
		((time_[0].tv_sec*1000000L) + (time_[0].tv_nsec/1000L)) ;
	}
	~CTimer() {

	}
private:
	timespec time_[2];
};

int main(int argc, char* argv[])
{
    // Parse command line.
    std::vector<uint32_t> blockCounts;
    std::vector<uint32_t> threadCounts;
    std::vector<uint32_t> iterations;
    bool nLogLevel=0;

    {
        int32_t opt;
        while ((opt = getopt(argc,argv,"b:t:i:")) != EOF){

            auto convert=[](std::vector<uint32_t>& v, const char* sz) {
                std::istringstream iss(sz);
                std::string s;
                while (std::getline(iss,s,',')) {
                    v.push_back(std::stoi(s));
                }
            };

            switch (opt){
            case 'b':
            {
                convert(blockCounts, optarg);
                break;
            }
            case 't':
            {
                convert(threadCounts, optarg);
                break;
            }
            case 'i':
            {
                convert(iterations, optarg);
                break;
            }
            case 'l':
                nLogLevel=std::stoi(optarg);
                break;
            default:
                return 1;
            }
        }
    }

    {
        cudaError_t nCudaError;
        nCudaError=cudaSetDevice(0);
        if (nCudaError) {
            return 1;
        }
        uint32_t nFlags;
        nCudaError=cudaGetDeviceFlags(&nFlags);
        if (nCudaError) {
            return 1;
        }
        std::cout << "Flags=" << nFlags << std::endl;
        nCudaError=cudaSetDeviceFlags(cudaDeviceScheduleYield);
        if (nCudaError) {
            return 1;
        }
    }

    // Run kernel on GPU.
    occupancy();

    SHA256Context context;
    context.set("abc");

    SHA256Context* cContext;
    cudaMalloc(&cContext, sizeof(SHA256Context));
    cudaMemcpy(cContext, &context, sizeof(SHA256Context), cudaMemcpyHostToDevice);

    // Iterate over our arguments.
    for (auto b : blockCounts) {
        for (auto t : threadCounts) {
            for (auto i : iterations) {
                //CTimer timer;
                cudaEvent_t start,stop;
                cudaEventCreate(&start);
                cudaEventCreate(&stop);
                cudaEventRecord(start,0);
                mh_sha256_kernel<<<b,t>>>(cContext, i);
                cudaEventRecord(stop,0);
                cudaEventSynchronize(stop);
                float nTimems;
                cudaEventElapsedTime(&nTimems, start, stop);
                const uint64_t nTimeus=nTime*1000.0;

                if (0 == nLogLevel) {
                    std::cout << b << ',' << t << ',' << i << ',' << (uint64_t(t)*b*i)/nTimeus << std::endl;
                }
                if (nLogLevel > 0){
                    std::cout << "Elapsed Time : " << std::dec << nTimeus << " us. "
                              << "Blocks=" << b
                              << " Threads=" << t
                              << " Iterations=" << i << ". "
                              << (uint64_t(t)*b*i)/nTimeus << " Mh/s." << std::endl;
                }
                if (nLogLevel > 1) {
                    cudaMemcpy(&context, cContext, sizeof(SHA256Context), cudaMemcpyDeviceToHost);
                    std::cout << context << std::endl;
                }
            }
        }
    }

    return 0;
}

