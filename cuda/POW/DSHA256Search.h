/*
 * DSHA256Search.h
 *
 *  Created on: Oct 26, 2018
 *      Author: paul
 */

#ifndef DSHA256SEARCH_H_
#define DSHA256SEARCH_H_

#include "SHA256Context.h"
#include "CudaUtils.h"
#include <vector>

class CDSHA256Search {
public:
    void run_sync(CSHA256Context& context, std::vector<uint32_t>& nonces);

    struct SHA256NonceData {
        enum {MAX_NONCES=7};
        uint32_t nSize;
        uint32_t nonces[MAX_NONCES];
    };
    struct SHA256Nonce : public SHA256NonceData {
        SHA256Nonce() {nSize=0;}
        __device__
        SHA256Nonce& operator=(const SHA256NonceData& d) { nSize=d.nSize; memcpy(nonces, d.nonces, sizeof(nonces)); return *this;}
    };
    enum {MAX_BLOCKS=1024};

private:

    CCudaGlobalMemoryTempl<CSHA256Context> context_;
    CCudaGlobalMemoryTempl<SHA256Nonce[MAX_BLOCKS]> nonces_;
    CCudaEvent start_, stop_;
};

#endif /* DSHA256SEARCH_H_ */
