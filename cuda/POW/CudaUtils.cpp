/*
 * CudaUtils.cpp
 *
 *  Created on: Oct 26, 2018
 *      Author: paul
 */
#include <chrono>
#include <thread>
#include <cuda_runtime.h>

#include "CudaUtils.h"

CCudaEvent::CCudaEvent() {
    checkCudaErrors(cudaEventCreate(&event_));
}

CCudaEvent::~CCudaEvent() {
    checkCudaErrors(cudaEventDestroy(event_));
}

CCudaSync::CCudaSync(CCudaEvent& start, CCudaEvent& stop) : start_(start), stop_(stop) {
    checkCudaErrors(cudaEventRecord(start_.get()));
}

uint32_t CCudaSync::wait() {

    checkCudaErrors(cudaEventRecord(stop_.get()));

    checkCudaErrors(cudaEventSynchronize(stop_.get()));
#if 0
    // Wait and poll.
    while (cudaErrorNotReady == cudaEventQuery(stop_.get())) {
        std::chrono::milliseconds timespan(100);
        std::this_thread::sleep_for(timespan);
    }
#endif

    float nTimems;
    checkCudaErrors(cudaEventElapsedTime(&nTimems, start_.get(), stop_.get()));
    return nTimems;
}

CCudaGlobalMemory::CCudaGlobalMemory(const uint32_t nSize) : nSize_(nSize) {
    checkCudaErrors(cudaMalloc(&p_, nSize));
}

CCudaGlobalMemory::~CCudaGlobalMemory() {
    if (p_) {
        checkCudaErrors(cudaFree(p_));
    }
}

void* CCudaGlobalMemory::get() {return p_;}
const void* CCudaGlobalMemory::get() const {return p_;}
uint32_t CCudaGlobalMemory::size() const {return nSize_;}


int cudaOccupancy_(std::ostream& o, const void* func, const uint32_t blockSize) {

    // These variables are used to convert occupancy to warps
    int device;
    cudaDeviceProp prop;

    int numBlocks=0;

    checkCudaErrors(cudaGetDevice(&device));
    checkCudaErrors(cudaGetDeviceProperties(&prop, device));
    checkCudaErrors(cudaOccupancyMaxActiveBlocksPerMultiprocessor(
        &numBlocks,
        func,
        blockSize,
        0));

    const uint32_t activeWarps = numBlocks * blockSize / prop.warpSize;
    const uint32_t maxWarps = prop.maxThreadsPerMultiProcessor / prop.warpSize;

   o << "name=" << std::dec << prop.name << std::endl
      << "numBlocks=" << numBlocks << std::endl
      << "sharedMemPerBlock=" << prop.sharedMemPerBlock << std::endl
      << "regsPerBlock=" << prop.regsPerBlock << std::endl
      << "maxThreadsPerMultiProcessor=" << prop.maxThreadsPerMultiProcessor << std::endl
      << "warpSize=" << prop.warpSize << std::endl
      << "activeWarps=" << activeWarps << std::endl
      << "maxWarps=" << maxWarps << std::endl
      << "Occupancy=" << (double)activeWarps / maxWarps * 100 << "%" << std::endl;

#if 0
    {
        int minGridSize;
        cudaOccupancyMaxPotentialBlockSize (
                &minGridSize,
                &blockSize,
                func
                );
        o << "minGridSize=" << minGridSize << std::endl
          << "blockSize=" << blockSize << std::endl;
    }
#endif
    return 0;
}
