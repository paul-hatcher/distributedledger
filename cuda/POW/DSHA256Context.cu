/*
 * DSHA256Context.cpp
 *
 *  Created on: Oct 26, 2018
 *      Author: paul
 */

#include "DSHA256Context.h"

// Code is inlined for CUDA performance.
// The optimizer makes a much better job
// of the kernel when all code is available.
