/*
 * DSHA256Context.h
 *
 *  Created on: Oct 26, 2018
 *      Author: paul
 */

#ifndef DSHA256CONTEXT_H_
#define DSHA256CONTEXT_H_

#include "SHA256Context.h"
#include "SHA256Impl.h"

class CDSHA256Context : public CSHA256Context {
public:

    __device__
    inline CDSHA256Context(const CSHA256Context* p);
    __device__
    inline void set(const uint32_t n, const uint32_t nOffset);
    __device__
    inline const uint32_t* getInput() const;
    __device__
    inline const uint32_t* getDigest() const;

private:

    enum {MAX_NONCE=4};
    uint32_t nIndex_= {0};
    uint32_t nonce_[MAX_NONCE];
};

__device__
CDSHA256Context::CDSHA256Context(const CSHA256Context* p) : CSHA256Context(p) {
}

__device__
void CDSHA256Context::set(const uint32_t n, const uint32_t nOffset) {
    input_[nOffset] = bswap(n);
}
#if 0
__device__
inline void CDSHA256Context::saveNonce(uint32_t nonce) {

    // Atomic increment.
    const int nIndex=atomicInc(&nIndex_, ~0);

    if (nIndex < MAX_NONCE) {
        nonce_[nIndex] = nonce;
    }
}
#endif

__device__
const uint32_t* CDSHA256Context::getInput() const {return input_;}

__device__
const uint32_t* CDSHA256Context::getDigest() const {return digest_;}

#endif /* DSHA256CONTEXT_H_ */
