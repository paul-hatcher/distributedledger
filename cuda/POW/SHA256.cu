
#include "SHA256.h"

CSHA256::CSHA256() {
    cudaMalloc((void**)&pSHA256Context_, sizeof(CSHA256Context));
}

CSHA256:: ~CSHA256() {
    cudaFree(pSHA256Context_);
}

CSHA256Context& CSHA256::getContext() {
    return context_;
}

const CSHA256Context& CSHA256::getContext() const {
    return context_;
}

#if 0
__global__
void sha256_kernel_search(SHA256Context*const p) {

    // Take a copy of incoming context. Will byte swap input.
    SHA256Context context(*p);
    const uint64_t nSearchSpace= 0x0100000000LL * 0x0100;
    const uint64_t nIterations = nSearchSpace / (gridDim.x*blockDim.x);
    uint64_t nonce = ((blockIdx.x * blockDim.x) + threadIdx.x) * nIterations;

    if (threadIdx.x < 4) {
        //printf ("%08X %08X %08X %08X\n", gridDim.x, blockDim.x, nIterations, nonce);
    }

    // Just iterate over given context.
    uint32_t digest[SHA256_DIGEST_WORDS];
    for (uint32_t i=0 ; i<nIterations; ++i, ++nonce) {
        context.reset();
        context.set(nonce & 0x0FFFFFFFF, 2);
        context.set(nonce >> 32, 3);
        sha256_single(context, digest);

        // Test the digest to see if we have a result.
        if (0 == (0xFF & digest[0])) {
            //p->saveNonce(nonce);
        }
    }
}

__global__
void sha256_kernel_test(SHA256Context*const p) {

    // Take a copy of incoming context. Will byte swap input.
    SHA256Context context(*p);
    //uint32_t digest[SHA256_DIGEST_WORDS];
    sha256_single(context, p->getDigest());
}
#endif

void CSHA256::kernel_search(uint32_t nBlocks, uint32_t nThreads, uint32_t nIterations) {
#if 0
    // Copy ourselves to CUDA memory.
    cudaMemcpy(pSHA256Context_, &context_, sizeof(SHA256Context), cudaMemcpyHostToDevice);

    // Then fire off the kernel.
    sha256_kernel_search<<<nBlocks,nThreads>>>(pSHA256Context_);

    // Copy context back.
    cudaMemcpy(&context_, pSHA256Context_, sizeof(SHA256Context), cudaMemcpyDeviceToHost);

    // We expect the caller to time events.
#endif
    }

void CSHA256::kernel_test() {

    CCudaSync sync(start_, stop_);

    // Launch kernel.

    sync.wait();
    // Wait for kernel to complete.

    // Do What's needed.

#if 0
    // Copy ourselves to CUDA memory.
    cudaMemcpy(pSHA256Context_, &context_, sizeof(SHA256Context), cudaMemcpyHostToDevice);

    // Then fire off the kernel.
    sha256_kernel_test<<<1,1>>>(pSHA256Context_);

    // Copy context back.
    cudaMemcpy(&context_, pSHA256Context_, sizeof(SHA256Context), cudaMemcpyDeviceToHost);

    // We expect the caller to time events.
#endif
    }

void CSHA256::occupancy(std::ostream& o) const {
//    occupancy_(o, sha256_kernel_test);
//    occupancy_(o, sha256_kernel_search);
}

int CSHA256::occupancy_(std::ostream& o, PFunction func) const {
    int numBlocks;        // Occupancy in terms of active blocks
    int blockSize = 512;

    // These variables are used to convert occupancy to warps
    int device;
    cudaDeviceProp prop;
    int activeWarps;
    int maxWarps;

    cudaGetDevice(&device);
    cudaGetDeviceProperties(&prop, device);

    cudaOccupancyMaxActiveBlocksPerMultiprocessor(
        &numBlocks,
        func,
        blockSize,
        0);

    o << "name=" << prop.name << std::endl
      << "numBlocks=" << numBlocks << std::endl
      << "sharedMemPerBlock=" << prop.sharedMemPerBlock << std::endl
      << "regsPerBlock=" << prop.regsPerBlock << std::endl
      << "maxThreadsPerMultiProcessor=" << prop.maxThreadsPerMultiProcessor << std::endl
      << "warpSize=" << prop.warpSize << std::endl
      ;

    activeWarps = numBlocks * blockSize / prop.warpSize;
    maxWarps = prop.maxThreadsPerMultiProcessor / prop.warpSize;

    o << "Occupancy: " << (double)activeWarps / maxWarps * 100 << "%" << std::endl;

    {
        int minGridSize;
        cudaOccupancyMaxPotentialBlockSize (
                &minGridSize,
                &blockSize,
                func,
                0,
                0
                );
        o << "minGridSize=" << minGridSize << std::endl
          << "blockSize=" << blockSize << std::endl
                  ;
    }
    return 0;
}

