/*
 * CudaUtils.h
 *
 *  Created on: Oct 26, 2018
 *      Author: paul
 */

#ifndef CUDAUTILS_H_
#define CUDAUTILS_H_

#include <ostream>
#include <inttypes.h>

template <typename T>
void check(T result, char const *const func, const char *const file, int const line) {
  if (result) {
    fprintf(stderr, "CUDA error at %s:%d code=%d(%s) \"%s\" \n", file, line,
            static_cast<unsigned int>(result), "?", func);
    cudaDeviceReset();
    exit(EXIT_FAILURE);
  }
}

// This will output the proper CUDA error strings in the event
// that a CUDA host call returns an error
#define checkCudaErrors(val) check((val), #val, __FILE__, __LINE__)

class CCudaEvent {
public:
    CCudaEvent();
    ~CCudaEvent();

    cudaEvent_t get() {return event_;}

private:
    cudaEvent_t event_;
};

class CCudaSync {
public:
    CCudaSync(CCudaEvent& start, CCudaEvent& stop);
    uint32_t wait();
private:
    CCudaEvent &start_, &stop_;
};

class CCudaGlobalMemory {
public:
    CCudaGlobalMemory(const uint32_t nSize);
    ~CCudaGlobalMemory();
    void* get();
    const void* get() const;
    uint32_t size() const;
private:
    void* p_={nullptr};
    const uint32_t nSize_;
};

template <class TYPE>
class CCudaGlobalMemoryTempl : public CCudaGlobalMemory {
public:
    CCudaGlobalMemoryTempl() : CCudaGlobalMemory(sizeof(TYPE)) {}
    TYPE* get() { return reinterpret_cast<TYPE*>(CCudaGlobalMemory::get());}
    const TYPE* get() const { return reinterpret_cast<const TYPE*>(CCudaGlobalMemory::get());}
};

extern int cudaOccupancy_(std::ostream& o, const void* func, const uint32_t blockSize);
template <class TYPE>
int cudaOccupancy(std::ostream& o, TYPE func, const uint32_t blockSize) {
    return cudaOccupancy_(o, (const void*)func, blockSize);
}

#endif /* CUDAUTILS_H_ */
