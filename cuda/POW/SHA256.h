/*
 * SHA256.h
 *
 *  Created on: Oct 23, 2018
 *      Author: paul
 */

#ifndef SHA256_H_
#define SHA256_H_

#include "SHA256Context.h"
#include "CudaUtils.h"

typedef void (*PFunction)(CSHA256Context*);

class CSHA256 {
public:
    CSHA256();
    ~CSHA256();

    CSHA256Context& getContext();
    const CSHA256Context& getContext() const;

    void kernel_search(uint32_t nBlocks, uint32_t nThreads, uint32_t nIterations);
    void kernel_test();

    void occupancy(std::ostream& o) const;

private:
    int occupancy_(std::ostream& o, PFunction func) const;

    CSHA256Context context_;
    CSHA256Context* pSHA256Context_;

    CCudaEvent start_, stop_;
};

#endif /* SHA256_H_ */
